import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { Email } from 'meteor/email';
import { check } from 'meteor/check';

export const LogLines = new Mongo.Collection('LogLines');


Meteor.methods({
  'LogLines.canShareWithUser'(username) {
    console.log('LogLines.canShareWithUser');
    console.log(username);
    if (Meteor.isServer) {
      if (! this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      let user = Meteor.users.findOne({'username': username})
      console.log(JSON.stringify(user));
      if (user) {
        return {
          _id: user._id,
          email: user.emails[0].address,
          username: user.username,
          email: user.emails[0].address
        };
      } else {
        return {};
      }
    }
  },
  'LogLines.shareLogLineWith'(userId, username, logLineId) {
    // console.log('LogLines.shareLogLineWith');
    // console.log(userId);
    // console.log(username);
    // console.log(logLineId);
    LogLines.update({_id : logLineId}, {$set : {sharedWith:[{userId : userId, username : username}]}});
  },
  'LogLines.sendEmail'(to, from, subject, text) {
    if (Meteor.isServer) {
      check([to, from, subject, text], [String]);
      // Let other method calls from the same client start running,
      // without waiting for the email sending to complete.
      this.unblock();
      Email.send({
        to: to,
        from: from,
        subject: subject,
        text: text
      });
    }
  }
});
