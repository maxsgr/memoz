import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import ReactDOM from 'react-dom';
import ReactMarkdown from 'react-markdown';

import { LogLines } from '../api/LogLines.js';

import LogLineMenu from './LogLineMenu.jsx';
import FontAwesome from 'react-fontawesome';

const APP_HOST_NAME = 'http://backtothinking.lombardstreet.io:8080';

export default class LogLine extends Component {

  constructor(props) {
    super(props);
    this.state = {showMenu: false};
  }

  handleDeleteLogLine(e) {
    e.preventDefault();
    if (confirm('Are you sure you want to delete this?')) {
      const logLineId = this.props.logLine._id;
      LogLines.remove({_id: logLineId});
    }
  }

  handleClick(e) {
    e.preventDefault();
    this.props.onClick(this.props.logLine._id);
  }

  handleOnMouseEnter(e) {
    e.preventDefault();
    this.setState({showMenu: true});
  }

  handleOnMouseExit(e) {
    e.preventDefault();
    this.setState({showMenu: false});
  }

  handleStopSharing(e) {
    e.preventDefault();
    LogLines.update({_id : this.props.logLine._id}, {$unset : {sharedWith : 1}});
  }

  getSharedWithList() {
    let userList = "";
    let i;
    let userArray = this.props.logLine.sharedWith;
    if (!userArray) return;
    for (i=0; i < userArray.length; ++i) {
      // (!userList)?  userList = ' ' : null;
      if (Meteor.user()){
        userList += (userArray[i].username === Meteor.user().username)? "Me " : userArray[i].username + " ";
      }
    }
    return userList;
  }

  isShared() {
    let userArray = this.props.logLine.sharedWith
    return  userArray && (userArray.length > 0);
  }
  isOwner() {
    return this.props.logLine.owner === Meteor.userId();
  }
  render() {

    return (
      <li id={this.props.logLine._id} className="log-line-item" >
        <div>
          <div className="log-line-head" onMouseEnter={this.handleOnMouseEnter.bind(this)} onMouseLeave={this.handleOnMouseExit.bind(this)} >
            {moment(this.props.logLine.createdAt).format('MMM DD hh:mm a')}&nbsp;
            {this.isShared() ?
              <span>
                {/*<FontAwesome name="user-circle-o"/>*/}
                &nbsp;
                {this.isOwner() ? <FontAwesome className="arrow-style" name="arrow-right"/> : '' }
              </span>: <FontAwesome name="lock"/>
            }
            {this.getSharedWithList()}
            {!this.isOwner() ? <FontAwesome className="arrow-style" name="arrow-left"/> : '' }
            <strong>{(this.isOwner())? "" : Meteor.users.findOne(this.props.logLine.owner).username + ' '}</strong>
            {this.isOwner() ?
              <span>
                <a className="show-menu" href="#" onClick={this.handleOnMouseEnter.bind(this)} >...</a>
                {this.state.showMenu ?
                  <span>&nbsp;&nbsp;
                    {this.isShared() ?
                      <a className="command" href="#" onClick={this.handleStopSharing.bind(this)}><FontAwesome name="hand-scissors-o" title="Stop sharing"/></a> : ''
                    }
                    <a className="command" href="#" onClick={this.handleClick.bind(this)}>Edit</a>
                    <a className="command" href="#" onClick={this.handleDeleteLogLine.bind(this)}>Del</a>
                    &nbsp;<label className="command-label">Share:</label>
                    <input type="text" className="command-input" placeholder="Thinker username here"
                      onKeyPress={(e) => {
                        const ENTER_KEYCODE = 13;
                        if (e.which == ENTER_KEYCODE) {
                          let userToShare = e.target.value;
                          e.target.value = "";
                          Meteor.call('LogLines.canShareWithUser', userToShare, (err, ret) => {
                            if (!err) {
                              if (ret && ret._id) {
                                let userIdList = _.pluck(this.props.logLine.sharedWith, 'userId');
                                if (userIdList.indexOf(ret._id) !== -1) {
                                  alert('You are already sharing with user!')
                                } else {
                                  Meteor.call('LogLines.shareLogLineWith', ret._id, ret.username, this.props.logLine._id, (err, innerRet) => {
                                    let text = Meteor.user().username + ' shared a new thought with you. Reat it at ' + APP_HOST_NAME;
                                    console.log('ret->' + ret);
                                    Meteor.call('LogLines.sendEmail', ret.email, 'BackToThinking<hello@lombardstreet.io>', 'A new though for you on BackToThinking', text);
                                  });
                                }
                              } else {
                                alert('Cannot share because this username is not in the database. Ask this guy to register first.');
                              }
                            } else {
                              alert('Something went wrong, dude!')
                            }
                          })
                        }
                      }}
                    />
                  </span> : ''
                }
              </span>: ''
            }
            </div>
          <ReactMarkdown className="log-line-content" source={this.props.logLine.text} softBreak="br" />
        </div>
      </li>
    );
  }
}

LogLine.PropTypes = {
  logLine: PropTypes.object.isRequired,
}
