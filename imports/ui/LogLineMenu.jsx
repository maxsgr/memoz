import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import { LogLines } from '../api/LogLines.js';

export default class LogLineMenu extends Component {

  render() {
    let menuStyle = {
      fontSize: '22px',
      fontFamily: 'Montserrat, sans-serif',
      backgroundColor: 'whitesmoke',
      borderRadius: '12px',
      padding: '1px 3px 2px 2px',
      color: 'gray'
    }

    return (
      <div>
        <span style={menuStyle}>.</span>
      </div>
    );
  }

}
