import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Textarea from 'react-textarea-autosize';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

import { LogLines } from '../api/LogLines.js';

import LogLine from './LogLine.jsx';
import EditLogLine from './EditLogLine.jsx';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { currentLogLineId: -1 };
    this.state = { currentLogLineText: 'empty'};
    this.state = { currentLogLineTotalChars: 0 };
    this.state = { isEditLogLineOpen: false };
  }

  handleSubmit(e) {
    e.preventDefault();
    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
    if (text !== "") {
      LogLines.insert({
        text: text,
        createdAt: new Date(),
        owner: Meteor.userId()
      });
      ReactDOM.findDOMNode(this.refs.textInput).value = '';
    }  else {
      alert('Please insert some text :)');
    }
  }

  handleKeys(e) {
    const ENTER_KEYCODE = 13;
    switch (e.which) {
      case ENTER_KEYCODE:
        if (e.ctrlKey) {
          e.preventDefault();
          this.handleSubmit(e);
        }
        break;
    }
  }

  renderLogLines() {
    return this.props.logLines.map((logLine) => (
      <LogLine key={logLine._id} logLine={logLine} inputText={ReactDOM.findDOMNode(this.refs.textInput)} totalChars={logLine.text.length}  onClick={(logLineId) => this.openEditLogLine(logLineId)}/>
    ))
  }

  componentDidMount() {
    ReactDOM.findDOMNode(this.refs.textInput).focus();
  }

  openEditLogLine(logLineId) {
    let text = LogLines.findOne({_id: logLineId}).text;
    this.setState({ currentLogLineId: logLineId });
    this.setState({ currentLogLineText: text });
    this.setState({ currentLogLineTotalChars: text.length });
    this.setState({ isEditLogLineOpen: true });
    window.scrollTo(0, 0);
    }

  closeEditLogLine() {
    this.setState({ isEditLogLineOpen: false });
  }

  render() {
    return (
      <div className="container">
      <EditLogLine logLineId={this.state.currentLogLineId} logLineText={this.state.currentLogLineText} logLineTotalChars={this.state.currentLogLineTotalChars} isOpen={this.state.isEditLogLineOpen} onClose={() => this.closeEditLogLine()} updateTotalChars={(totChars) => this.setState({currentLogLineTotalChars: totChars})}  />
      <header>
        <div className="head-title">Memoz</div>
        <AccountsUIWrapper />
        {this.props.currentUser ?
        <div>
          <form className="new-logline" onSubmit={this.handleSubmit.bind(this)} >
            <Textarea ref="textInput" placeholder="Write a new memo here" onKeyPress={this.handleKeys.bind(this)} ></Textarea>
          </form>
          <div className="inline-help">CTRL + ENTER TO PUBLISH or <button className="button-default button-small" onClick={this.handleSubmit.bind(this)}>SAVE</button></div>
        </div> : ''
        }
      </header>
      {this.props.currentUser ?
        <ul className="log-items">
        {this.renderLogLines()}
        </ul> :
        <span>Welcome to <strong >Memoz</strong>, a tool for your private memos. Take a note or write down an idea immediatley simply using Markdown to format your text.
        <p>Some hints: **bold**, _emboss_, - list item, ... </p></span>
      }
      </div>

    )
  }
}

App.PropTypes = {
  logLines: PropTypes.array.isRequired,
}

export default createContainer(() => {
  return {
    logLines: LogLines.find({$or : [{owner: Meteor.userId()}, {'sharedWith.userId': Meteor.userId()}]},{sort: {createdAt: -1}}).fetch(),
    currentUser: Meteor.userId()
  }
}, App);
