import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import ReactMarkdown from 'react-markdown';
import Textarea from 'react-textarea-autosize';
import { Meteor } from 'meteor/meteor';
import { LogLines } from '../api/LogLines.js';

export default class EditLogLine extends Component {

  constructor(props) {
    super(props);
    // this.state = {totalCharacters: 0};
  }

  // componentDidMount() {
  //   this.setState({totalCharacters: (this.props.logLineId)? LogLines.findOne({_id: this.props.logLineId}).text.length : 0})
  // }

  close(e) {
    e.preventDefault()

    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  handleSave(e) {
    e ? e.preventDefault() : null;
    const text = e.target.value;
    LogLines.update({_id: this.props.logLineId}, {$set: {
        text: text,
        createdAt: new Date()
      }
    });
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  handleKeys(e) {
    const ENTER_KEYCODE = 13;
    const ESCAPE_KEYCODE = 27;

    switch (e.which) {

      case ENTER_KEYCODE:
        if (e.ctrlKey) {
          e.preventDefault();
          this.handleSave(e);
        }
        break;
    }
  }

  updateContent(e) {
    // this.setState({content: e.target.value});
    LogLines.update({_id: this.props.logLineId}, {$set: {
        text: e.target.value,
        createdAt: new Date()
      }
    });
    this.props.updateTotalChars(e.target.value.length);
    // this.setState({totalCharacters: e.target.value.length});
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    if (this.props.isOpen === false)
      return null;

    let editTextareaStyle = {
      fontFamily: 'Work Sans, sans-serif',
      fontSize: '0.7em',
      width: '100%',
      background: 'transparent',
      border: 'none',
      borderBottom: '1px dotted gray',
      width: '100%',
      padding: '12px 5px',
      marginBottom: '4px',
      resize: 'none',
      lineHeight: '155%',
      outline: 'none'
    }

    let editorContainerStyle = {
       position: 'absolute',
       top: '100px',
       left: '15%',
       height: '100%',
      //  transform: 'translate(-50%, -50%)',
       zIndex: '9999',
       background: '#fff',
       width: '70%',
       fontFamily: 'Work Sans, sans-serif',
       fontSize: '1.4em'
       }

    let backdropStyle = {
      position: 'fixed',
      width: '100%',
      height: '100%',
      top: '0px',
      left: '0px',
      zIndex: '9998',
      background: 'rgba(255, 255, 255, 0.97)'
    }

    let menuBarStyle = {
      fontSize: 'small'
    }

    return (
      <div>
        <div style={backdropStyle}>
        </div>
        <div style={editorContainerStyle} >
          <form className="edit-logline" onSubmit={this.handleSave.bind(this)} >
             <Textarea ref="textEdit" placeholder="Write some text here" style={editTextareaStyle} onKeyPress={this.handleKeys.bind(this)} onChange={this.updateContent.bind(this)} onLoad={this.updateContent.bind(this)} defaultValue={this.props.logLineText}></Textarea>
          </form>
          { /* <button className="button-default" onClick={(e) => this.close(e)}>Cancel</button><button className="button-default" onClick={this.handleSave.bind(this)}>Save & Close</button> */ }
          <div>
            <span style={menuBarStyle}>{this.props.logLineTotalChars} characters</span>
            <button className="button-default button-small" onClick={(e) => this.close(e)}>DONE</button>
          </div>
        </div>
      </div>
    );

  }
}
